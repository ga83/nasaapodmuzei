package com.tux.nasaapodmuzei;

import android.net.Uri;
import android.util.Log;

import com.google.android.apps.muzei.api.provider.Artwork;
import com.google.android.apps.muzei.api.provider.MuzeiArtProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class NasaApodArtProvider extends MuzeiArtProvider {

    private static final String TAG = NasaApodArtProvider.class.getName();
    private static long READ_TIMEOUT = 60 * 1000;
    private static long WRITE_TIMEOUT = 10 * 1000;

    private static String API_ENDPOINT = "https://api.nasa.gov/planetary/apod";
    private static String API_KEY = "!";


    public NasaApodArtProvider() {

    }

    @Override
    protected void onLoadRequested(boolean initial) {
        Artwork artwork = getNewArtwork();

        if(artwork != null) {
            addArtwork(artwork);
        }
    }


    private com.google.android.apps.muzei.api.provider.Artwork getNewArtwork() {

        // first date of apod
        Date minDate = new Date(1995 - 1900, 6, 16);
        long minTimestamp = minDate.getTime();

        // today
        Date maxDate = new Date();
        long maxTimestamp = maxDate.getTime();

        // random date
        long randomTimeStamp = ThreadLocalRandom.current().nextLong(minTimestamp, maxTimestamp);
        Date date = new Date(randomTimeStamp);

        String randomDateString = 1900 + date.getYear() + "-" + String.format("%02d", date.getMonth() + 1) + "-" + String.format("%02d", date.getDay());

        HttpUrl.Builder urlBuilder = HttpUrl.parse(API_ENDPOINT).newBuilder();
        urlBuilder.addQueryParameter("api_key", API_KEY);
        urlBuilder.addQueryParameter("date", randomDateString);

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        OkHttpClient client = builder
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .build();

        Response response;
        String jsonResponse;
        JSONObject jsonObject;

        boolean success = true;
        String hdurl = "";
        String imageTitle = "";
        String copyright = "";
        String imageContextUrl = "";
        Uri uri = null;
        Uri contextUri = null;

        try {
            response = client.newCall(request).execute();
            jsonResponse = response.body().string();

            if(jsonResponse.contains("hdurl") == false) {
                throw new APODFatalException("hdurl key not present");
            }

            jsonObject = new JSONObject(jsonResponse);

            hdurl = jsonObject.getString("hdurl");

            if(hdurl.equals("") == true) {
                throw new APODFatalException("hdurl value empty");
            }

            uri = Uri.parse(hdurl);

            imageTitle = jsonObject.getString("title");
            copyright = jsonObject.getString("copyright");

            contextUri = Uri.parse(imageContextUrl);    // find out if there's a way to get this
        }
        catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
            e.printStackTrace();
        }
        catch (JSONException e) {
            Log.e(TAG, "JSONException: " + e.getMessage());
            e.printStackTrace();
        }
        catch (APODFatalException e) {
            success = false;
            Log.e(TAG, "APODFatalException: " + e.getMessage());
            e.printStackTrace();
        }

        if (success == true) {
            Artwork artwork;

            if(copyright != null) {
                artwork = new Artwork
                        .Builder()
                        .title(imageTitle)
                        .byline(copyright)
                        .persistentUri(uri)
                        .build();
            } else {
                artwork = new Artwork
                        .Builder()
                        .title(imageTitle)
                        .persistentUri(uri)
                        .build();
            }

            return artwork;
        }
        else {
            return null;
        }
    }

    private class APODFatalException extends Throwable {
        String errorMessage;

        public APODFatalException(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        @Override
        public String getMessage() {
            return errorMessage;
        }
    }
}
